import React  from 'react';
import {
    Navbar,

    NavbarBrand,
    Nav,
    NavItem,

} from 'reactstrap';
import {NavLink as RouterLink} from "react-router-dom";
import style from "./Header.module.css"

const Header = (props) => {
    const {homeLogo, navItems} = props;

    const itemList = navItems.map((item) => {
        return (
            <NavItem key={item.url} className={style.navItem}>
                <RouterLink exact={item.exact}
                            activeClassName={style.active}
                            to={item.url}
                            className="nav-link">
                    {item.text}
                </RouterLink>
            </NavItem>
        )
    })

    return (
        <div className={style.navBar}>
            <div class="text-center pt-3">
                <RouterLink to="/"><h3>CLOUDCOMPUTING2020 - LOCAL HUB VACCINE MONITOR</h3></RouterLink>
            </div>

            <Navbar expand="md" full light>
                <div className="container text-center justify-content-center">


                        <Nav className="text-center" navbar>
                            {itemList}
                        </Nav>





                </div>
            </Navbar>
        </div>
    );
}

export default Header;
