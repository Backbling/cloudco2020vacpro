import React from "react";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";


function MainTemplate(props) {
    const {children, netflixLink, instagramLink, navItems, homeLogo} = props;
    return (
        <>
        <Header navItems = {navItems} homeLogo={homeLogo}/>
            {children}
        <Footer
                netflixLink = {netflixLink}
                instagramLink = {instagramLink}

        />
        </>

    );
}

export default MainTemplate;
