import React, {useEffect, useState} from "react";
import {Card, CardText, CardSubtitle, CardBody, CardTitle} from "reactstrap";


function DatiNazionali(props) {
    const [datiNazionali, setDatiNazionali] = useState([]);

    useEffect(() =>
        {

            fetch('http://localhost:8081/VisualizzaDatiNazionali')
                .then(res=>res.json())
                .then(res => {
                    setDatiNazionali(res.data);
                })
                .catch((error) => console.log("Error" + error));

        },  []


    )
    return (
        <div className="container">

            {datiNazionali.map(el => (

                <div className="row justify-content-center">
                    <Card>
                        <CardBody>
                            <CardTitle tag="h5">{el.area}</CardTitle>
                            <CardSubtitle tag="h7" className="mb-2 text-muted">Ultimo aggiornamento: {el.ultimo_aggiornamento}</CardSubtitle>
                            <CardText>{el.dosi_consegnate}/{el.dosi_somministrate} {el.percentuale_somministrazione}%</CardText>

                        </CardBody>
                    </Card>
                </div>
            ))}

        </div>






    );
}

export default DatiNazionali;
