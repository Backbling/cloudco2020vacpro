import React, {useState,useEffect} from "react";
import { Table } from 'reactstrap';

function Prenotazioni(props) {

    const [prenotazioniData, setPrenotazioniData] = useState([]);

    useEffect(() =>
        {

            fetch('http://localhost:8081/VisualizzaPrenotazionaliLocali')
                .then(res=>res.json())
                .then(res => {
                    setPrenotazioniData(res);
                })
                .catch((error) => console.log("Error" + error));

        }, []


    )



    return (
        <div className="container">
            <Table striped>
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Codice fiscale</th>
                    <th>Data prenotazione</th>
                    <th>Allergie</th>
                </tr>
                </thead>
                <tbody>
                {prenotazioniData.map(el => (
                    <tr>
                        <td>{el.nome}</td>
                        <td>{el.cf}</td>
                        <td>{el.dataprenotazione}</td>
                        <td>{el.allergie}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>


    );
}

export default Prenotazioni;
