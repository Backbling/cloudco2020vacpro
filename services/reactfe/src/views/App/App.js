import React from "react";
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import MainTemplate from "../../components/MainTemplate/MainTemplate";
import Prenotazioni from "../Prenotazioni/Prenotazioni";
import DatiNazionali from "../DatiNazionali/DatiNazionali";
import Vaccinati from "../Vaccinati/Vaccinati";


function App() {
  const nav = [
    {url: "/prenotazioni", text: "Prenotazioni", exact: true},
      {url: "/vaccinati", text: "Vaccinati", exact: false},
    {url: "/datinazionali", text: "Dati nazionali", exact: true}

  ]
  return (
      <BrowserRouter>
        <MainTemplate
            navItems={nav}
        >
          <Switch>
            <Route exact path="/prenotazioni" component={Prenotazioni} />
            <Route exact path="/vaccinati" component={Vaccinati} />
            <Route exact path="/datinazionali" component={DatiNazionali} />
          </Switch>

        </MainTemplate>
      </BrowserRouter>


  );
}

export default App;
