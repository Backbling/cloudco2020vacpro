import React, {useState,useEffect} from "react";
import {Table, Form, Row, Col, FormGroup, Label, Input, Button} from "reactstrap";

function Vaccinati(props) {
    const [vaccinatiData, setVaccinatiData] = useState([]);



    const handlxeSubmit = event => {
        event.preventDefault();
        alert('You have submitted the form.')

        //fetch('http://localhost:8081/InserisciVaccinato?nome=ciro&cf=codicefiscale&data=21/11/2020')
    }
    const handleSubmit = (event) => {
        event.preventDefault();
        let today = new Date().toLocaleDateString()
        let nome = event.target[0].value;
        let cf = event.target[1].value
      
        fetch(`http://localhost:8081/InserisciVaccinato?nome=${nome}&cf=${cf}&data=${today}`)
        fetch('http://localhost:8081/VaccinatiLocali')
            .then(res=>res.json())
            .then(res => {
                setVaccinatiData(res);
            })
            .catch((error) => console.log("Error" + error));
    }

    useEffect(() =>
        {

            fetch('http://localhost:8081/VaccinatiLocali')
                .then(res=>res.json())
                .then(res => {
                    setVaccinatiData(res);
                })
                .catch((error) => console.log("Error" + error));

        }

, []
    )
    return (
        <div className="container">



            <Form onSubmit={handleSubmit}>
                <Row form>
                    <Col md={3}>
                        <FormGroup>

                            <Input type="text" name="email" id="exampleEmail" placeholder="Nome" />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>

                            <Input type="text" name="password" id="examplePassword" placeholder="Codice fiscale" />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <Button className="">Inserisci vaccinato</Button>
                </Col>

                </Row>

            </Form>




            <Table striped>
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Codice fiscale</th>
                    <th>Data vaccinazione</th>
                </tr>
                </thead>
                <tbody>
                {vaccinatiData.map(el => (
                    <tr>
                        <td>{el.nome}</td>
                        <td>{el.cf}</td>
                        <td>{el.data}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        </div>


    );
}

export default Vaccinati;
