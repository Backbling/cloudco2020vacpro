from flask import Flask
from flask import request
from flask import jsonify
from pymongo import MongoClient
import json
import os
import requests

MONGODBCONT_HOST = os.environ["MONGODBCONT_HOST"]
MONGODBCONT_USER = os.environ["MONGODBCONT_USER"]
MONGODBCONT_PWD = os.environ["MONGODBCONT_PWD"]
prenotconthost = os.environ["PRENOTCONT_HOST"]
prenotconthost = prenotconthost + ":" + os.environ["PRENOTCONT_PORT"]


app = Flask(__name__)


client = MongoClient("mongodb://"+MONGODBCONT_USER+":"+MONGODBCONT_PWD+"@"+MONGODBCONT_HOST+":27017/") 

db = client['dbvaccinati']
collection = db['vaccinatilocali']



@app.route('/VisualizzaDatiNazionali', methods=['GET'])
def VisualizzaDatiNazionali():
	with open("/datas/dati/vaccini-summary-latest.json") as test_file:
		data = json.load(test_file)
	


	response = jsonify(data)
	response.headers.add('Access-Control-Allow-Origin', '*')

	return response

@app.route("/VisualizzaPrenotazionaliLocali", methods=['GET'])
def VisualizzaPrenotazionaliLocali():
	r = requests.get('http://'+prenotconthost+'/Prenotazioni')

	response = jsonify(json.loads(r.text))
	print("prima")
	print(r.text)
	print("dopo")

	response.headers.add('Access-Control-Allow-Origin', '*')
	print(response)
	return response

@app.route("/InserisciVaccinato", methods=['GET', 'POST'])
def InserisciVaccinato():
	nome = request.args.get('nome')
	cf = request.args.get('cf')
	data = request.args.get('data')

	entry = {
	  "nome": nome,
	  "cf": cf,
	  "data": data
	  }
	db.collection.insert_one(entry).inserted_id

	response = jsonify({"success": True, "nome": nome, "cf": cf, "data":data})
	response.headers.add('Access-Control-Allow-Origin', '*')

	return response

@app.route("/VaccinatiLocali", methods=['GET'])
def VaccinatiLocali():
	array = []
	for a in db.collection.find():
		array.append({"nome": a["nome"], "cf": a["cf"], "data": a["data"]})

	response = jsonify(array)
	response.headers.add('Access-Control-Allow-Origin', '*')

	return response


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8081)	
