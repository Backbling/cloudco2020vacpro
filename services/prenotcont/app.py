from flask import Flask
from flask import jsonify
from flask import request
from pymongo import MongoClient
import json
import os

MONGODBCONT_HOST = os.environ["MONGODBCONT_HOST"]
MONGODBCONT_USER = os.environ["MONGODBCONT_USER"]
MONGODBCONT_PWD = os.environ["MONGODBCONT_PWD"]


app = Flask(__name__)


#client = MongoClient("mongodb://root:root_password@172.18.0.4:27017/") 

client = MongoClient("mongodb://"+MONGODBCONT_USER+":"+MONGODBCONT_PWD+"@"+MONGODBCONT_HOST+":27017/") 



db = client['dbprenotati']
collection = db['prenotazioni']


@app.route('/', methods=['GET'])
def index():
	return "Servizio prenotazioni - MONGODBCONT_OST = " + MONGODBCONT_HOST

@app.route('/CheckUtentePresente', methods=['GET'])
def CheckUtentePresente():
	cf = request.args.get('cf')
	if db.collection.count_documents({ 'cf': cf }, limit = 1) != 0:
		return "Prenotazione trovata"
	return "Prenotazione non presente per questo codice fiscale"

@app.route("/InsertPrenotazioneUtente", methods=['GET'])
def InsertPrenotazioneUtente():
	nome = request.args.get('nome')
	cf = request.args.get('cf')
	dataprenotazione = request.args.get('dataprenotazione')
	allergie = request.args.get('allergie')

	entry = {
	  "nome": nome,
	  "cf": cf,  
	  "dataprenotazione": dataprenotazione,
	  "allergie": allergie

	  }
	db.collection.insert_one(entry).inserted_id
	return 'Prenotazione inserita correttamente per ' + nome + cf + dataprenotazione + allergie

@app.route("/Prenotazioni", methods=['GET'])
def Prenotazioni():
	array = []
	for a in db.collection.find():
		array.append({"nome": a["nome"], "cf": a["cf"], "dataprenotazione": a["dataprenotazione"], "allergie": a["allergie"]})

	#response = jsonify(array)
	#response.headers.add('Access-Control-Allow-Origin', '*')

	return json.dumps(array)#response

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5051)	
